interface PaymentType {
  date: Date;
  ammount: number;
}

export class InvestPerformanceResultDto {
  totalPerformance: number;
  totalPayment: number;
  firstPayment: PaymentType;
  payments: PaymentType[];
}
