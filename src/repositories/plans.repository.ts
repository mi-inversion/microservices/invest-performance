import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { PlanDto } from '../dto/plan.dto';

@Injectable()
export class PlansRepository {
  constructor(
    @InjectModel('InvestPerformancePlan') public readonly planModel: Model<any>,
  ) {}

  async create(planDto: PlanDto) {
    return await this.planModel.create(planDto);
  }

  async update(planDto: PlanDto) {
    return await this.planModel.updateOne({ _id: planDto.id }, planDto);
  }

  async getAll() {
    return await this.planModel.find();
  }

  async delete(planDto: PlanDto) {
    return await this.planModel.deleteOne({ _id: planDto.id });
  }
}
