import { Controller, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientProxy, MessagePattern } from '@nestjs/microservices';
import { PlanDto } from './dto/plan.dto';
import { InvestPerformanceInputsDto } from './dto/invest-performance-inputs.dto';

const SERVICE_NAME = 'investPerformance';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('SERVICES') private readonly client: ClientProxy,
  ) {}

  @MessagePattern({ service: SERVICE_NAME, cmd: 'createPlan' })
  create(planDto: PlanDto) {
    const response = this.appService.create(planDto);
    this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'planCreated' },
        { request: planDto, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'updatePlan' })
  update(planDto: PlanDto) {
    const response = this.appService.update(planDto);
    this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'performancePlanUpdated' },
        { request: planDto, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'deletePlan' })
  delete(planDto: PlanDto) {
    const response = this.appService.delete(planDto);
    this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'planDeleted' },
        { request: planDto, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'getPlans' })
  getAll() {
    const response = this.appService.getAll();
    this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'plansConsulted' },
        { request: {}, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({
    service: SERVICE_NAME,
    cmd: 'generateInvestPerformance',
  })
  generateInvestPerformance(data: InvestPerformanceInputsDto) {
    const response = this.appService.generateInvestPerformance(data);
    this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'investPerformanceConsulted' },
        { request: {}, response },
      )
      .toPromise();
    return response;
  }
}
