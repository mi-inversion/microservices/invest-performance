import { Module, Inject, OnApplicationBootstrap } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { PlanSchema } from './schemas/plan.schema';
import { PlansRepository } from './repositories/plans.repository';
@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('MONGODB_URI'),
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forFeature([
      { name: 'InvestPerformancePlan', schema: PlanSchema },
    ]),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    PlansRepository,
    {
      provide: 'SERVICES',
      useFactory: config => {
        return ClientProxyFactory.create({
          transport: Transport.NATS,
          options: {
            url: config.get('NATS_HOST'),
          },
        });
      },
      inject: [ConfigService],
    },
  ],
})
export class AppModule implements OnApplicationBootstrap {
  constructor(@Inject('SERVICES') private readonly client: ClientProxy) {}
  async onApplicationBootstrap() {
    await this.client.connect();
  }
}
