import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PlansRepository } from './repositories/plans.repository';
import { PlanDto } from './dto/plan.dto';
import { InvestPerformanceInputsDto } from './dto/invest-performance-inputs.dto';
import { InvestPerformanceResultDto } from './dto/invest-performance-result-dto';
import { RpcException } from '@nestjs/microservices';
import moment = require('moment');

@Injectable()
export class AppService {
  constructor(private readonly planRepository: PlansRepository) {}

  async create(planDto: PlanDto) {
    return await this.planRepository.create(planDto);
  }
  async update(planDto: PlanDto) {
    return await this.planRepository.update(planDto);
  }
  async delete(planDto: PlanDto) {
    return await this.planRepository.delete(planDto);
  }
  async getAll() {
    return await this.planRepository.getAll();
  }

  async generateInvestPerformance({
    ammount: stringAmmount,
    dateToOpen,
    planId,
  }: InvestPerformanceInputsDto): Promise<InvestPerformanceResultDto> {
    const plan: PlanDto = await this.planRepository.planModel.findOne({
      _id: planId,
    });
    // tslint:disable-next-line: radix
    const ammount = Number.parseInt(stringAmmount);
    if (ammount > plan.maxInvest || ammount < plan.minInvest) {
      throw new RpcException('amount outbound limit');
    }

    const totalPerformance =
      ammount * (plan.monthlyRate / 100) * plan.planDuration;

    const totalPayment = totalPerformance + ammount;

    const paymentsNumber = plan.planDuration / plan.performanceDelivery;

    const monthlyPayment = totalPerformance / paymentsNumber;
    const firstPaymentDate = moment(dateToOpen).add(1, 'M');

    const firstPayment = {
      date: firstPaymentDate.toDate(),
      ammount: monthlyPayment,
    };

    const payments = [];
    for (let i = 1; i < paymentsNumber; i++) {
      payments.push({
        date: moment(dateToOpen)
          .add(i * plan.performanceDelivery, 'M')
          .toDate(),
        ammount: monthlyPayment,
      });
    }

    return {
      firstPayment,
      payments,
      totalPayment,
      totalPerformance,
    };
  }
}
